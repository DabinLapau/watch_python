#!/usr/bin/env python3
import sys
import time
import logging
import subprocess
import os
import asyncio
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler, FileSystemEventHandler


class ModifiedEventHandler(FileSystemEventHandler):
    def __init__(self, runner, arg):
        self.runner = runner
        self.arg = arg

    def on_modified(self, event):
        super(ModifiedEventHandler, self).on_modified(event)

        # get normalized path
        arg = os.path.normpath(self.arg)
        src_path = os.path.normpath(event.src_path)

        # if target file is modified, run runner
        if event.is_directory is False and arg == src_path:
            asyncio.run(self.runner.run())


class Runner:
    def __init__(self, src_path):
        # stores handles for spawned subprocesses
        self.proc = None
        self.src_path = src_path

    async def run(self):
        # if there is a running process, kill it
        if self.proc:
            self.proc.kill()
        # spawn a process, store the handle in proc
        self.proc = await asyncio.create_subprocess_shell(f"python {self.src_path}")


def main():
    # basic logging configs
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s - %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S",
    )

    # you need to specify a file
    if len(sys.argv) < 1:
        print("please specify a file to watch")
        return

    filename = sys.argv[1]

    # check if filename is valid
    if not os.path.isfile(filename):
        print("not a valid file")
        return

    # returns (parent directoy, filename)
    path = os.path.split(filename)[0]
    # if empty, its probably the current directory
    if not path:
        path = "."

    # manages subprocesses
    runner = Runner(os.path.normpath(filename))
    # runs runner when target file is modified
    event_handler = ModifiedEventHandler(runner, filename)
    # setup and start file system observer
    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()

    # accpet keyboard interrupt
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()


if __name__ == "__main__":
    main()
