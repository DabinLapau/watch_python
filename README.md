# Run python file on save

runs python files on save for you.

## Requirements

watch dog is required.
you can download it with

```sh
pip install watchdog
```

## Usage

```sh
python watch.py sub.py
```

sub.py can be any target file.

now, if you edit sub.py in a different editor,
it should be rerun on save
